/*
@DERECHOS RESERVADOS 2015
@File                 : recorteporareas.js
@Description          : Recorte de imagenes por �reas.
@Input Parameters     : 
@Output Parameters    : 
@Author               : @LyraSoftware
@Enginner             : @cmagana
@Created              : 06/03/2015
@CallSyntax           :
@Notes                : Selecci�n de diferentes �reas dentro de una imagen para:
						guardar �reas resaltadas en imagen nueva mediante guardar_imagen.php, 
						guarda datos y coordenadas de imagen de base de datos,
						recuperar �reas desde base de datos y otras funcionalidades.

--------------------------- Modifications ------------------------------
No.     Author          Date        Description
------------------------------------------------------------------------ 	
1       @cmagana        19/03/2015  Se complementa plug-in con funciones nuevas, ajustes y comentarios.		
*/
window.onload = function(){

	/* CONFIGURACI�N */
	var config = 
	{
		/* Id de imagen a aplicarse funcionalidad de recorte */
		"IDrecortar": "recorteporareas",
		/* Crear imagen */
		"crearImagen": {
			"habilitar": true,
			"rutaArchSubeImagen": "/recorteporareas/guardar_imagen.php",
			"rutaAbsDescRecortes": "C:/xampp/htdocs/recorteporareas/recortes/"
		},
		/* Guardar en BD */
		"guardarDB": {
			"habilitar": true,
			"rutaSetRecorte": "../pro/set_recorte.r"
		},
		/* Mostrar botones con acciones */
		"mostrarBotones": {
			"habilitar": true,
			"rutaRelDescRecortes": "http://localhost/recorteporareas/recortes/",
			"rutaGetRecorte": "../rep/get_recorte"
		},
		/* Recuperar coordenadas en carga inicial de p�gina */
		"recuperaCoords": {
			"habilitar": false,
			"rutagetCoords": "../rep/get_coordenadas"
		},
		/* Valores RGBA para selecci�n de �reas */
		"rgbaSelecc": "250,70,70,0.3",
		/* Valores RGBA para fondo de canvas */
		"rgbaCanvas": "0,0,0,0.6",
		/* Detalles de procesos en consola de java */
		"console": true
	}
	/* fin. CONFIGURACI�N */
	
	/* Toma imagen de DOM con el ID especificado */
	var	imagen1 = document.getElementById(config.IDrecortar);

	/* Crea copia de imagen para utilizarse en superposici�n */
	imagen_limpia     = new Image(); 
	imagen_limpia.src = imagen1.src;
	
	/* Se crean: objeto que guardar� informaci�n de imagen para ser guardada en BD, 
				 arreglo con coordenadas de �reas seleccionadas y 
				 variable que indica carga de p�gina inicial */
	var oImagen  = Object.create(null),
	    array    = [],
		comienza = true;
	
	/* Agrega seleccionador de �reas */			
	var html = '<style type="text/css">' +				
					'#rec_selecc {'  + 
						' background-color: rgba(' + config.rgbaSelecc + ');'  + 
					'}' +
				'</style>' + 
				'<div style="position: absolute; width: 0px; height: 0px; z-index: 5;" class="mod marchingants2" id="rec_selecc">'  + 
					'<div class="inner">'  + 
						'<div class="hd"></div>'  + 
						'<div class="bd"></div>'  + 
						'<div class="ft"></div>'  + 
					'</div>'  + 
				'</div>'  + 
				'<canvas id="canvas1" width="' + imagen1.width + '" height="' + imagen1.height + '" style="width:'+imagen1.width+'px;height:'+imagen1.height+'px;"></canvas>';
				
	var div = document.createElement("div");              
	div.setAttribute("id","recorte");
	document.body.appendChild(div);                    
	document.getElementById("recorte").innerHTML = html;
	/* fin. Agrega seleccionador de �reas */		

	function CanvasState(canvas) {

		/* CORTAR IMAGEN */
		function cortarImagen(){
		
			var rutaImagenNueva,
				canvas_nueva = document.getElementById("canvas1"),
				img = canvas_nueva.toDataURL("image/png").replace('data:image/png;base64,', '');
			
			/* CREA IMAGEN PNG A PARTIR DE CANVAS */
			function crearImagen() {
				var xmlHttpReq = false;       
				if (window.XMLHttpRequest) {
					ajax = new XMLHttpRequest();
				}
				else if (window.ActiveXObject) {
					ajax = new ActiveXObject("Microsoft.XMLHTTP");
				}
			    ajax.open('POST', config.crearImagen.rutaArchSubeImagen,false);
			    ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
			    ajax.onreadystatechange = function() {
					if (ajax.readyState == 4) {
						alert("El recorte se ha creado y guardado en disco como una imagen nueva.");
						rutaImagenNueva = ajax.responseText;
						if(config.console) console.log(">>Se ha guardado la imagen en servidor dentro de la siguiente ruta: "+ rutaImagenNueva);
					}
				}
			    ajax.send("inputDataUrl="+img+"&inputSrc="+imagen1.src+"&rutaDescRecortes="+config.crearImagen.rutaAbsDescRecortes);
			}
			if(config.crearImagen.habilitar) crearImagen();
			else if(config.console) console.log(">>No esta habilitado crear nueva imagen PNG: crearImagen = false.");
			/* fin. CREA IMAGEN PNG A PARTIR DE CANVAS */

			/* GUARDAR EN DB INFORMACI�N DE RECORTE */
			var img_nomb = imagen1.src,
				viPos    = img_nomb.lastIndexOf("/");
			img_nomb = img_nomb.substring(viPos+1);

			var vImagen = JSON.stringify(oImagen);

			if(config.console) {
				console.log(">>Datos a env�ar para guardar en base de datos.");
				console.log("SRC de imagen a recortar: "+imagen1.src);
				console.log("Ruta donde recorte se almacen�: "+rutaImagenNueva);
				console.log("T�tulo de im�gen (atributo title): "+imagen1.title);
				console.log("Nombre de imagen (imagen1.jpg): "+img_nomb);
				console.log("Coordenadas de �reas seleccionadas: "+vImagen);
			}

			if(config.guardarDB.habilitar){
				function guardarDB() {
					var xmlHttpReq = false;       
					if (window.XMLHttpRequest) {
						ajax = new XMLHttpRequest();
					}
					else if (window.ActiveXObject) {
						ajax = new ActiveXObject("Microsoft.XMLHTTP");
					}
					ajax.open('POST',config.guardarDB.rutaSetRecorte,false);
					ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
					ajax.onreadystatechange = function() {
						if (ajax.readyState == 4) {
							if(config.console) console.log(">>Intenta guardar en BD.");
							if(+ajax.responseText === 1){
								if(config.console) console.log(">>Los datos de la imagen se han guardado en la base de datos.");
							} else {
								if(config.console) console.log(">>Un error ocurri� al tratar de guardar en la base de datos.");
							}
						}
					}
					ajax.send("src_orig="+imagen1.src+"&src_reco="+rutaImagenNueva+"&img_titl="+imagen1.title+"&img_nomb="+img_nomb+"&coords="+vImagen);
				}
				guardarDB();
				
				/* BOT�N DE ACCI�N */
				if(config.mostrarBotones.habilitar){
					/* ABRIR CON RUTA DESDE BASE DE DATOS  */
					if(config.console) console.log(">>Se encuentra habilitado mostrar bot�n para abrir en nueva ventana imagen guardada en BD.");

					var elemento=document.getElementById('abrirImagen');
					if (elemento !==  null)
					elemento.parentNode.removeChild(elemento);
					
					var boton = document.createElement("button");      
					boton.setAttribute("id","abrirImagen");
					var botonTexto = document.createTextNode("Obtener y abrir iruta de imagen guardada en BD."); 
					boton.appendChild(botonTexto); 
					document.body.appendChild(boton);
					var abrirImagen = document.getElementById('abrirImagen');
					abrirImagen.addEventListener('click', function (e) {
					
						/* Recupera imagen de base de datos */
						if(config.console) {
							console.log(">>Recupera imagen de base de datos.");
							console.log("Src de recorte:" + config.mostrarBotones.rutaRelDescRecortes + img_nomb);
						}
						function recuperaImagen() {
							var xmlHttpReq = false;       
							if (window.XMLHttpRequest) {
								ajax = new XMLHttpRequest();
							}
							else if (window.ActiveXObject) {
								ajax = new ActiveXObject("Microsoft.XMLHTTP");
							}
							ajax.open('POST',config.mostrarBotones.rutaGetRecorte,false);
							ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
							ajax.onreadystatechange = function() {
								if (ajax.readyState == 4) {
									if(config.console) console.log(">>Se ha recuperado la imagen por su ruta de alojo recuperada de la base de datos: " + config.mostrarBotones.rutaRelDescRecortes + ajax.responseText);
									var w=window.open();
									w.document.open();
									w.document.write('<img title="Nueva imagen" src="' + config.mostrarBotones.rutaRelDescRecortes + ajax.responseText + '"/>');
								}
							}
							ajax.send("src_orig="+imagen1.src);
						}
						recuperaImagen();
					});
				}
			}else{
				if(config.console) console.log(">>No se guardo en Base de datos, funcionalidad desactivada: config.guardarDB = false.");
			}
			/* fin. GUARDAR EN DB INFORMACI�N DE RECORTE */		
		}
		
		/* Inicia canva y contexto */
		this.canvas = canvas;
		this.width = canvas.width;
		this.height = canvas.height;
		this.ctx = canvas.getContext('2d');
		
		/* Ajustes para iniciar y evitar imperfecciones al seleccionar �reas */
		var stylePaddingLeft, stylePaddingTop, styleBorderLeft, styleBorderTop;
		if (document.defaultView && document.defaultView.getComputedStyle) {
			this.stylePaddingLeft = parseInt(document.defaultView.getComputedStyle(canvas, null)['paddingLeft'], 10) || 0;
			this.stylePaddingTop = parseInt(document.defaultView.getComputedStyle(canvas, null)['paddingTop'], 10) || 0;
			this.styleBorderLeft = parseInt(document.defaultView.getComputedStyle(canvas, null)['borderLeftWidth'], 10) || 0;
			this.styleBorderTop = parseInt(document.defaultView.getComputedStyle(canvas, null)['borderTopWidth'], 10) || 0;
		}
		var html = document.body.parentNode;
		this.htmlTop = html.offsetTop;
		this.htmlLeft = html.offsetLeft;
		this.dragging = false;
		this.dragoffx = 0;
		this.dragoffy = 0;
		this.mx = 0;
		this.my = 0;		
				
		/*  myState tomar� a this  */
		var myState = this;

		/* Rellena canvas con imagen oscurecida */
		myState.fillSombra();
		
		/* Intenta tomar imagen limpia (superpuesta) dimensiones de canvas (no probado) */
		imagen_limpia.width = canvas.width;
		imagen_limpia.height = canvas.height;
		
		/* Paso de contexto de canvas a objeto */
		cxt1=this.ctx; 	
				
		/* Recupera �reas con coordenadas */
		if(config.recuperaCoords.habilitar){
			function agregaAreaExis(x,y,Width,Height){
				cxt1.drawImage(imagen_limpia, x, y, Width, Height, x, y, Width, Height);
			}		
			function recuperaCoordenadas(coords){
				if(coords !== "" && coords !=="{}"){
					yImagen = JSON.parse(coords);
					if(yImagen.coord.length>0){
						var i=0;			
						for(i;i<yImagen.coord.length;i++){
							agregaAreaExis(yImagen.coord[i][0],yImagen.coord[i][1],yImagen.coord[i][2],yImagen.coord[i][3]);
							/* Agrega coordenadas existentes a objeto que guarda coordenadas */
							array.push([+yImagen.coord[i][0],+yImagen.coord[i][1],+yImagen.coord[i][2],+yImagen.coord[i][3]]);
							oImagen.coord = array;
						}
						if(config.console) console.log(">>Se han recuperado las coordenadas desde la base de datos para esta imagen.");				
					}
				}
			}
			/* Recupera coordenadas si existen */
			function recuperaCoordS() {
				var xmlHttpReq = false;       
				if (window.XMLHttpRequest) {
					ajax = new XMLHttpRequest();
				}
				else if (window.ActiveXObject) {
					ajax = new ActiveXObject("Microsoft.XMLHTTP");
				}
				ajax.open('POST',config.recuperaCoords.rutagetCoords,false);
				ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
				ajax.onreadystatechange = function() {
					if (ajax.readyState == 4) {
						recuperaCoordenadas(ajax.responseText);
					}
				}
				ajax.send("src_orig="+imagen1.src);
			}
			if(comienza) recuperaCoordS();	
		}
		/* fin. Recupera �reas con coordenadas */

		/* Agrega �reas */
		function agregaArea(e){
			var ant = document.getElementById('rec_selecc');
			var left = ant.style.left;
			var top = ant.style.top;
			var left = ((+left.substring(0, left.length - 2)) - (+canvas.offsetLeft) + 7) + "px";
			var top = ((+top.substring(0, top.length - 2)) - (+canvas.offsetTop) + 11) + "px";
			var Width = ant.style.width;
			var Height = ant.style.height;
			var sourceX = left.substring(0, left.length - 2) - 5;
			var sourceY = top.substring(0, top.length - 2);
			var sourceWidth = Width.substring(0, Width.length - 2);
			var sourceHeight = Height.substring(0, Height.length - 2);
			/* Evita guardar �reas fallidas */
			if(+sourceWidth  !== 0 || +sourceHeight !== 0 ){
				array.push([+sourceX,+sourceY,+sourceWidth,+sourceHeight]);
				oImagen.coord = array;
				cxt1.drawImage(imagen_limpia, sourceX, sourceY, sourceWidth, sourceHeight, sourceX, sourceY, sourceWidth, sourceHeight);
			}
		}
		
		/* BOTONES DE ACCI�N */
		if(config.mostrarBotones.habilitar){
			
			/* MOSTRAR CANVAS CON SOLO AREAS SELECCIONADAS */
			if(config.console) console.log(">>Se encuentra habilitado mostrar bot�n para desplegar imagen solo con �reas resaltadas.");

			var elemento=document.getElementById('verImagenLimpia');
			if (elemento !==  null)
			elemento.parentNode.removeChild(elemento);
			
			var boton = document.createElement("button");      
			boton.setAttribute("id","verImagenLimpia");
			var botonTexto = document.createTextNode("Mostrar solo �reas seleccionadas"); 
			boton.appendChild(botonTexto); 
			document.body.appendChild(boton);
			var verImagen = document.getElementById('verImagenLimpia');
			verImagen.addEventListener('click', function (e) {
		
				myState.clear(e);
				if(oImagen.coord.length>0){
					var i=0;			
					for(i;i<oImagen.coord.length;i++){
						agregaAreaExis(oImagen.coord[i][0],oImagen.coord[i][1],oImagen.coord[i][2],oImagen.coord[i][3]);
					}
					if(config.console) console.log(">>Se recuperan coordenadas desde objeto oImagen.");
				}

			});

			/* MOSTRAR CANVAS CON FONDO SOMBREADO */
			if(config.console) console.log(">>Se encuentra habilitado mostrar bot�n para desplegar imagen con fondo sombreado.");
			
			var elemento=document.getElementById('verImagenSombra');
			if (elemento !==  null)
			elemento.parentNode.removeChild(elemento);
			
			var boton = document.createElement("button");      
			boton.setAttribute("id","verImagenSombra");
			var botonTexto = document.createTextNode("Mostrar con fondo sombreado"); 
			boton.appendChild(botonTexto); 
			document.body.appendChild(boton);
			var verImagen = document.getElementById('verImagenSombra');
			verImagen.addEventListener('click', function (e) {
		
				myState.fillSombra(e);
				if(oImagen.coord.length>0){
					var i=0;			
					for(i;i<oImagen.coord.length;i++){
						agregaAreaExis(oImagen.coord[i][0],oImagen.coord[i][1],oImagen.coord[i][2],oImagen.coord[i][3]);
					}
					if(config.console) console.log(">>Se recuperan coordenadas desde objeto oImagen.");						
				}

			});

			/* RECORTAR */
			if(config.crearImagen.habilitar){
				if(config.console) console.log(">>Se encuentra habilitado mostrar bot�n para recortar.");
				
				var elemento=document.getElementById('recortarImagen');
				if (elemento !==  null)
				elemento.parentNode.removeChild(elemento);
				
				var boton = document.createElement("button");      
				boton.setAttribute("id","recortarImagen");
				var botonTexto = document.createTextNode("Recortar"); 
				boton.appendChild(botonTexto); 
				document.body.appendChild(boton);
				var verImagen = document.getElementById('recortarImagen');
				verImagen.addEventListener('click', function (e) {

					cortarImagen();

				});
			}
		}else{
			if(config.console) console.log(">>No se muestran botones de acci�n, funcionalidad desactivada: mostrarBotones = false.");
		}
		/* fin. BOTONES DE ACCI�N */

		canvas.addEventListener('selectstart', function (e) {
			e.preventDefault();
			return false;
		}, false);

		canvas.addEventListener('mousedown', function (e) {
			var mouse = myState.getMouse(e);
			this.mx = mouse.x;
			this.my = mouse.y;
			var ant = document.getElementById('rec_selecc');
			ant.style.left = (this.mx + 10) +  (+canvas.offsetLeft) + "px";
			ant.style.top = (+(this.my) +  (+canvas.offsetTop)) + "px";
			ant.style.width = (0) + "px";
			ant.style.height = (0) + "px";
			myState.dragging = true;
		}, true);

		canvas.addEventListener('mousemove', function (e) {
			if (myState.dragging) {
			  var mouse = myState.getMouse(e);
			  width = (mouse.x < this.mx) ? this.mx - mouse.x : mouse.x - this.mx;
			  height = (mouse.y < this.my) ? this.my - mouse.y : mouse.y - this.my;
			  var ant = document.getElementById('rec_selecc');
			  ant.style.left = ((mouse.x < this.mx) ? (mouse.x + 0 + canvas.offsetLeft) : (this.mx + 0 + canvas.offsetLeft) ) + "px";
			  ant.style.top = ((mouse.y < this.my) ? (mouse.y - 1 + canvas.offsetTop) : (this.my - 1 + canvas.offsetTop) ) + "px";
			  ant.style.width = (width - 6) + "px";
			  ant.style.height = (height - 6) + "px";
			}
		}, true);
		
		canvas.addEventListener('mouseup', function (e) {
			myState.dragging = false;
			/* Agrega �rea */
			agregaArea(e);
		}, true);

		canvas.addEventListener('dblclick', function (e) {
			/* Limpia objetos y array y recarga canvas */
			comienza = false;
			oImagen = {};
			yImagen = {};
			array = new Array();
			myState.clear(e);
			myState.fillSombra(e);
			myState.dragging = false;
		}, true);
			
		function KeyCode(e) {
			/* Presionando (ctrl + x) */
			var keycode;  
			if (window.event) keycode = window.event.keyCode;  
			else if (e) keycode = e.which;  
			var e = e || window.event;  
			if(keycode==88 && e.ctrlKey){  
				/* Corta imagen */
				cortarImagen();
			}  
		}  
		document.onkeydown = KeyCode;  
	}
	
	CanvasState.prototype.clear = function () {
		/* limpia canvas */
		this.ctx.clearRect(0, 0, this.width, this.height);
	}
	
	CanvasState.prototype.fillSombra = function () {
		/* Rellena canvas con imagen oscurecida */
		this.ctx.drawImage(imagen1, 0, 0, this.width, this.height);	
		this.ctx.fillStyle = "rgba(" + config.rgbaCanvas + ")";
		this.ctx.fillRect(0, 0, this.width, this.height); 	
	}

	CanvasState.prototype.getCanvasHandle = function (e) {
		return this.ctx;
	}

	CanvasState.prototype.getMouse = function (e) {
		/* Obtiene coordenadas del cursor sobre canvas */
		var element = this.canvas,
		offsetX = - 0,
		offsetY = - 0,
		mx, my;

		if (element.offsetParent !== undefined) {
			do {
				offsetX += element.offsetLeft;
				offsetY += element.offsetTop;
			} while ((element = element.offsetParent));
		}

		offsetX += this.stylePaddingLeft + this.styleBorderLeft + this.htmlLeft;
		offsetY += this.stylePaddingTop + this.styleBorderTop + this.htmlTop;

		mx = e.pageX - offsetX;
		my = e.pageY - offsetY;

		return {
			x: mx,
			y: my
		};
	}
	/* Inicia CanvasState */
	var myCanvas = new CanvasState(document.getElementById("canvas1"));
	
	/* Esconde imagen tomada, solo se utiliza de referencia (colocado aqu� por problema con Chrome) */
	imagen1.style.display = "none";
	imagen1.style.height = 0;
}