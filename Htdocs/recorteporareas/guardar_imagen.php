<?php
/* ------------------------------------------------------------------------
@DERECHOS RESERVADOS 2014
@File                 : guardar_imagen.php
@Description          : Coloca una imagen en servidor.
@Input Parameters     : 
@Output Parameters    : 
@Author               : @LyraSoftware
@Enginner             : @cmagana
@Created              : 09/03/2015
@CallSyntax           :
@Notes                : 

--------------------------- Modifications ------------------------------
No.    Author          Date        Description
------------------------------------------------------------------------

----------------------------------------------------------------------*/

$data = $_POST['inputDataUrl'];
$data = str_replace(' ', '+', $data);
$nombre = $_POST['inputSrc'];
$nombre =  substr($nombre,strripos($nombre,"/")+1);
$rutaDescRecortes = $_POST['rutaDescRecortes'];
$file = $rutaDescRecortes.$nombre;
file_put_contents($file, base64_decode($data));
echo $file;
?>